FROM node:current-bullseye

WORKDIR /app
COPY . /app/
RUN apt-get update && \
    apt-get install -y ffmpeg v4l2loopback-dkms v4l2loopback-utils gstreamer1.0-plugins-base gstreamer1.0-plugins-good yarn && \
    apt-get autoclean
RUN modprobe snd-aloop pcm_substreams=1 && \
    modprobe v4l2loopback exclusive_caps=1