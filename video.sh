#!/bin/bash
# usage: ./video0.sh url

set -e

trap 'kill $(jobs -p); exit 1' TERM
trap 'kill $(jobs -p); exit 2' INT

rm -f .raw && mkfifo .raw

python3 -m yt_dlp --no-playlist "$1" -o - > .raw &

ffmpeg -re -i .raw -vf 'scale=1280:720,fps=30' -pix_fmt rgb24 -f v4l2 /dev/video0 \
    -f alsa -ac 2 -ar 44100 -c:a pcm_s16le hw:0,1,0 &

wait $!
