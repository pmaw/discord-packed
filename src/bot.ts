import { Client } from "discord.js";
import { Texer } from "./texer";
import Player from "./player";
import Errors from './text';

export class Bot {
    private client: Client;
    private token: string;
    private init: boolean;
    private texer: Texer;
    private player: Player;

    constructor(token: string, prefix: string) {
        this.token = token;
        this.init = false;
        this.client = new Client();
        this.texer = new Texer(prefix);
        this.player = new Player;
    }

    private printUsername() {
        return `${this.client.user.username}#${this.client.user.discriminator}`
    }

    public initialize() {
        this.clientInit();
        this.texerInit();
        this.init = true;
    }
    
    private clientInit() {
        this.client.on('ready', () => {
            console.info(`Logged in as ${this.printUsername()}`)
        });
        this.client.on('message', msg => {
            if (msg.author.bot || msg.author.id == this.client.user.id) return;
            this.texer.parse(msg);
        });
    }
    
    private texerInit() {
        this.texer.on('', async (msg) => {
            if (!msg.member.voiceChannel) {
                msg.channel.send(Errors.REQUESTER_NOT_ROOM);
                return;
            }
            const txt = msg.content, vc = msg.member.voiceChannel;
            const vid = await this.player.searchYoutube(txt);
            this.player.playYoutube(vc, vid);
        });
    }

    public async login() {
        if (this.init) this.client.login(this.token);
    }
}