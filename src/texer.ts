import { Message } from "discord.js";

export interface FunctionCommand {
    readonly name: string;
    readonly fn: (msg: Message) => void;
}

export class Texer {
    private prefix: string;
    private cmds: FunctionCommand[];

    constructor(prefix: string) {
        this.prefix = prefix;
        this.cmds = [];
    }

    public parse(msg: Message) {
        const txt = msg.content;
        console.info(`[${msg.author.username}]: ${txt}`);
        this.emit(txt.substr(this.prefix.length), msg);
    }

    private emit(command: string, msg: Message) {
        let is_found = false;
        this.cmds.forEach(cmd => {
            if (is_found) return;
            if (cmd.name == command) {
                cmd.fn(msg);
                is_found = true;
            }
        });
        if (is_found) return;
        this.cmds.forEach(cmd => {
            if (is_found) return;
            if (cmd.name == '') {
                cmd.fn(msg);
                is_found = true;
            }
        });
    }

    public on(command: string, fn: (msg: Message) => void) {
        let is_found = false;
        this.cmds.forEach(cmd => {
            if (is_found) return;
            if (cmd.name == command) is_found = true;
        });
        if (is_found) return;
        this.cmds.push({ name: command, fn: fn });
    }
}