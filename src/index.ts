import { Bot } from "./bot";
import Constants from "./const";

(async () => {
    const bot = new Bot(Constants.DISCORD_TOKEN, Constants.BOT_PREFIX);
    bot.initialize()
    await bot.login();
})();
