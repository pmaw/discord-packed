import { Video } from "./player";

export default {
    REQUESTER_NOT_ROOM: 'อยู่ในห้องแล้วค่อยขอครับ 8;p',
    VIDEO_NOT_FOUND: 'ไม่เจอ!',
    VIDEO_WILL_PLAY: (v: Video) => {return `กำลังจะเล่น ${v.name}!`},
    VIDEO_WILL_STOP: `หยุด!`,
}