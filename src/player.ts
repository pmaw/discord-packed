import Constants from './const';
import { google } from 'googleapis';
import { OAuth2Client } from 'google-auth-library';
import { Client, VoiceChannel } from 'discord.js';
import * as child_process from 'child_process';
import * as puppeteer from 'puppeteer';

export interface Video {
    name: string,
    link: string,
};

interface ClientProp {
    browser: puppeteer.Browser;
    page: puppeteer.Page;
    guild_id: string;
    voice_id: string;
    joined: boolean;
};

export default class Player {
    google_oauth: OAuth2Client;
    process: child_process.ChildProcessWithoutNullStreams;
    client: ClientProp;

    constructor() {
        this.google_oauth = new google.auth.OAuth2();
        this.google_oauth.apiKey = Constants.YOUTUBE_TOKEN;
        this.process = null;
        this.client = {
            browser: null,
            page: null,
            guild_id: null,
            voice_id: null,
            joined: false,
        }
    }

    async openDiscordClient() {
        this.client.browser = await puppeteer.launch({ headless: false, userDataDir: './.data' });
        if (!this.client.page) this.client.page = (await this.client.browser.pages())[0]; // Use the default tab instead of making a new one.
        await this.client.page.goto('https://discord.com/', { waitUntil: 'networkidle0' });
    }
    
    async closeDiscordClient() {
        if (this.client.browser) this.client.browser.close();
    }
    
    async joinVoiceChannel(guild_id: string, voice_id: string) {
        if (!this.client.browser) return;
        if (!this.client.page) this.client.page = (await this.client.browser.pages())[0];
        this.client.page.goto(`https://discord.com/channels/${guild_id}/`, { waitUntil: 'networkidle0' });
        const voiceChannelSelector = `a[data-list-item-id="channels___${voice_id}"]`;
        await this.client.page.waitForSelector(voiceChannelSelector, { timeout: 0 });
        await this.client.page.evaluate(v => document.querySelector(v).click(), voiceChannelSelector);
        this.client.guild_id = guild_id;
        this.client.voice_id = voice_id;
        this.client.joined = true;
    }
    
    async turnOnVideo() {
        if (!this.client.browser || !this.client.joined) return;
        if (!this.client.page) this.client.page = (await this.client.browser.pages())[0];
        const btnName = 'button[aria-label="Turn On Camera"]';
        await this.client.page.waitForSelector(btnName, { timeout: 0 });
        await this.client.page.evaluate(e => {
            const btn = document.querySelector(e);
            if (btn) btn.click()
        }, btnName);
    }
    
    async turnOffVideo() {
        if (!this.client.browser || !this.client.joined) return;
        if (!this.client.page) this.client.page = (await this.client.browser.pages())[0];
        const btnName = 'button[aria-label="Turn Off Camera"]';
        await this.client.page.waitForSelector(btnName, { timeout: 0 });
        await this.client.page.evaluate(e => {
            const btn = document.querySelector(e);
            if (btn) btn.click()
        }, btnName);
    }

    async searchYoutube(term: string): Promise<Video> {
        const service = google.youtube('v3');
        const results = await service.search.list({
            auth: this.google_oauth,
            part: ['snippet'],
            q: term,
            maxResults: 1,
        });
        if (results.status != 200) return {
            name: 'ริกโรล',
            link: 'https://www.youtube.com/watch?v=dQw4w9WgXcQ'
        }; // default is rick rolled
        const result = results.data.items[0];
        return {
            name: result.snippet.title,
            link: `https://www.youtube.com/watch?v=${result.id.videoId}`
        }
    }

    async playYoutube(channel: VoiceChannel, video: Video): Promise<void> {
        const gu_id = channel.guild.id, vc_id = channel.id;
        if (!this.client.browser) await this.openDiscordClient();
        if (this.process) this.process.kill('SIGTERM');
        this.process = child_process.spawn('./video.sh', [video.link]);
        this.process.on('close', () => {
            this.process.kill('SIGTERM');
        });
        await this.joinVoiceChannel(gu_id, vc_id);
        await this.turnOnVideo();
    }
};