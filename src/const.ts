export default {
    DISCORD_TOKEN: process.env.DISCORD_TOKEN,
    YOUTUBE_TOKEN: process.env.YOUTUBE_TOKEN,
    BOT_PREFIX: process.env.BOT_PREFIX ? process.env.BOT_PREFIX : '!',
};